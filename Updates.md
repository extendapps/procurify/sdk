# Update Guide

This guide will update as we iterate over versions

## v2.8 to v2.9

### Changelog:
 * handle Vendor/Account Updates to Procurify

### Notes:
 * When updating the bundle, please ensure that the 'Do Not Update Deployment' is set for all scripts

### Updating custom code:
 1. Clone the client repo
 2. Ensure all commits have been pushed to the repo
 3. Add a 'remote' to you GIT config
	 * Right-click the project
	 * Navigate to Git > Repository > Remotes
	 * Click the '+' to add a Remote
		 * name: SDK
		 * URL: https://gitlab.com/extendapps/procurify/sdk.git
 4. Pull the latest code from the SDK
	 * Right-click the project
	 * Navigate to Git > Repository > Pull
	 * Change the 'Remote' to the new SDK you added in Step #2
	 * Click 'refresh' next to the Remote (circular arrows)
		 * this will update the 'Branches to merge' window
	 * Choose 'SDK/master' from the 'Branches to merge' window
	 * Click 'Pull'
	 * New 'Merge' dialog box will open
		 * On each file listed, choose 'Accept Theirs'
			 * this will Overwrite the local files with the latest SDK version
				 * don't worry, the original files are still in the repository
 5. Restore original customizations overtop of the newly imported files
	 * for each file
		 * Right-click file
		 * Navigate to Git > Compare with Branch
			 * Choose origin/master
		 * Incorporate previous customizations into the new local version
			 * these changes should align with the original custom mapping requirements (usually found in the README.md file)
 6. Don't commit/push your changes to the repo just yet

### Updating the bundle

 1. Ensure your code has been updated (but not deployed)
 2. Start NetSuite bundle update to v2.9.18
	 * Ensure 'Do Not Update Deployments' is used
3. Publish your code changes via SDF
4. Commit code changes to the repo

### Post update review

 1. Ensure the Settings look good
 2. Review the status and schedule of each import/export process
	 * Imports (Purchase Order/Item Receipts)
		 * these should be scheduled to run every 15mins every day, all day (ensure the start time is 12:00am)
	 * Exports (Accounts/Vendors)
		 * these should be scheduled to run once, nightly. (ensure the start time is 12:00am)
