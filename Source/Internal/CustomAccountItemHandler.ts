/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as record from 'N/record';
import * as search from 'N/search';
import * as log from 'N/log';
import * as config from 'N/config';
import * as https from 'N/https';

export const createAccountItem = (options: CreateAccountItemOptions): void => {
    // Create a Noninventory Purchase Item
    let foundProcurifyItemId = -1;
    const procurifyItemName = `PFY - ${options.accountRecord.getValue({fieldId: 'acctnumber'})} - ${options.accountRecord.getValue({fieldId: 'acctname'})}`; // PFY - <Acct Num> - <Acct Name>

    search.create({
        type: search.Type.NON_INVENTORY_ITEM,
        filters: [
            ['subtype', 'anyof', 'Purchase'],
            'AND',
            ['itemid', 'is', procurifyItemName]
        ]
    }).run().each(result => {
        foundProcurifyItemId = +result.id;
        return false; // Should only be one
    });

    if (foundProcurifyItemId > 0) {
        options.Created(foundProcurifyItemId);
    } else {
        const procurifyItem: record.Record = record.create({
            type: record.Type.NON_INVENTORY_ITEM,
            defaultValues: {
                subtype: 'Purchase'
            },
            isDynamic: true
        });
        procurifyItem.setValue({fieldId: 'externalid', value: procurifyItemName});
        procurifyItem.setValue({fieldId: 'itemid', value: procurifyItemName});
        procurifyItem.setValue({fieldId: 'expenseaccount', value: options.accountRecord.id});

        if (options.taxScheduleId && +options.taxScheduleId > 0) {
            procurifyItem.setValue({fieldId: 'taxschedule', value: options.taxScheduleId});
        }

        if (isOneWorld()) {
            log.audit('createAccountItem - subsidiary', options.accountRecord.getValue({fieldId: 'subsidiary'}));

            // Need to account for the concept that only a select number of subsidires are choosen.
            if (Array.isArray(options.accountRecord.getValue({fieldId: 'subsidiary'}))) {
                const selectedSubsidiaries: number[] = <number[]>options.accountRecord.getValue({fieldId: 'subsidiary'});
                log.audit('createAccountItem - selectedSubsidiaries', selectedSubsidiaries);
                const subsidiaryIdFilter: string[] = ['internalid', 'anyof'];
                const subsidiaryFilter: string[] = subsidiaryIdFilter.concat(selectedSubsidiaries.map(String));
                const searchFilters: any[] = [];
                searchFilters.push(subsidiaryFilter);
                searchFilters.push('AND');
                searchFilters.push(['iselimination', 'is', 'F']);
                searchFilters.push('AND');
                searchFilters.push(['isinactive', 'is', 'F']);
                const nonEliminationSubsidiaries: number[] = [];
                search.create({
                    type: search.Type.SUBSIDIARY,
                    filters: searchFilters
                }).run().each(result => {
                    nonEliminationSubsidiaries.push(+result.id);
                    return true;
                });
                log.audit('createAccountItem - nonEliminationSubsidiaries', nonEliminationSubsidiaries);
                procurifyItem.setValue({fieldId: 'subsidiary', value: nonEliminationSubsidiaries});
            } else {
                log.audit('createAccountItem - single subsidiary', options.accountRecord.getValue({fieldId: 'subsidiary'}));
                procurifyItem.setValue({fieldId: 'subsidiary', value: options.accountRecord.getValue({fieldId: 'subsidiary'})});
            }
            procurifyItem.setValue({fieldId: 'includechildren', value: options.accountRecord.getValue({fieldId: 'includechildren'})});
        }

        try {
            options.Created(procurifyItem.save());
        } catch (e) {
            options.Failed(e);
        }
    }
};

export interface AccountItemCreatorHandler {
    createAccountItem(options: CreateAccountItemOptions): void;
}

interface CreateAccountItemOptions {
    accountRecord: record.Record;
    taxScheduleId: number;

    Created(recordId: number): void;

    Failed(clientResponse: https.ClientResponse): void;
}

const isOneWorld = (): boolean => {
    const accountingPreferences = config.load({type: config.Type.ACCOUNTING_PREFERENCES});
    const maxSub = +accountingPreferences.getValue({fieldId: 'MAXSUBSIDIARIES'});
    return !isNaN(maxSub);
};
