/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import * as record from 'N/record';
import {IImportHandler, IPurchaseOrderCreateHandlerOptions, IPurchaseOrderDeleteHandlerOptions, IPurchaseOrderUpdateHandlerOptions} from '@extendapps/procurifytypings/Interfaces/IImportHandler';
import * as search from 'N/search';
import {ProcurifySDK} from '@extendapps/procurifytypings/ProcurifySDK';
import {LineItem, PurchaseOrder} from '@extendapps/procurifyapi/Objects/PurchaseOrder';
import {Address} from '@extendapps/procurifyapi/Objects/Address';
import * as error from 'N/error';

const upsert = (options: IPurchaseOrderCreateHandlerOptions | IPurchaseOrderUpdateHandlerOptions, recordObj: record.Record, isUpdate = false): record.Record => {
    log.audit('NetSuitePurchaseOrderHandler - upsert - options', options);
    const purchaseOrder: PurchaseOrder = options.object;

    recordObj.setValue({fieldId: 'trandate', value: options.procurify.procurifyDatetoNSDate(purchaseOrder.transaction_date)});

    if (purchaseOrder.created_by && purchaseOrder.created_by.email && purchaseOrder.created_by.email.length > 0) {
        options.procurify.recordLookup({
            lookingForValue: undefined, // Only needed for Vendor Lookup
            searchOptions: {
                type: search.Type.EMPLOYEE,
                filters: [['email', 'is', purchaseOrder.created_by.email], 'AND', ['isinactive', 'is', 'F']]
            },
            isMandatory: false,
            Found: employeeResult => {
                recordObj.setValue({fieldId: 'employee', value: +employeeResult.id});
            }
        });
    }

    // options.procurify.manageSegment({segment: search.Type.DEPARTMENT, name: purchaseOrder.line_items[0].department.name.trim(), recordObj: recordObj});
    // options.procurify.manageSegment({segment: search.Type.CLASSIFICATION, filters: [['name', 'is', 'test']], recordObj: recordObj}); // Based on CustomFields
    // options.procurify.manageSegment({segment: search.Type.LOCATION, name: purchaseOrder.line_items[0].location.name.trim(), recordObj: recordObj});

    if (purchaseOrder.due_date) {
        recordObj.setValue({fieldId: 'duedate', value: options.procurify.procurifyDatetoNSDate(purchaseOrder.due_date)});
    }

    // Attempt to create a unique TRAN ID for NetSuite
    let newTranId: string = options.procurify.Configuration.Prefix !== null && options.procurify.Configuration.Prefix.length > 0 ? `${options.procurify.Configuration.Prefix}-` : '';

    // tslint:disable-next-line:prefer-conditional-expression
    if (purchaseOrder.purchase_order_no && purchaseOrder.purchase_order_no.length > 0) {
        newTranId = `${newTranId}${purchaseOrder.purchase_order_no}`;
    } else {
        newTranId = `${newTranId}PF${purchaseOrder.id}`;
    }
    recordObj.setValue({fieldId: 'tranid', value: newTranId}); // If the 'override' is not checked, NetSuite will ignore this attempt to set tranid

    if (purchaseOrder.memo && purchaseOrder.memo.length > 0) {
        recordObj.setValue({fieldId: 'memo', value: purchaseOrder.memo});
    }

    // Attempt to remove lines
    if (isUpdate) {
        // Gather list of PFY line ids from the existing NetSuite Purchase Order
        const existingLineIds: number[] = getExistingLineIds(recordObj, 'item');
        // Gather list of PFY line ids from the incoming Purchase Order update
        const incomingLineIds: number[] = purchaseOrder.line_items.map(lineItem => {
            return lineItem.id;
        });

        // Filter down to the missing Line Ids (these lines were removed from the PFY PO and need to be removed from the NetSuite PO)
        const removedLineIds: number[] = existingLineIds.filter(existingLineItemId => incomingLineIds.indexOf(existingLineItemId) < 0);

        removedLineIds.forEach(removedLineId => {
            removeLine(recordObj, 'item', removedLineId);
        });
    }

    purchaseOrder.line_items.forEach(lineItem => {
        upsertLine(options.procurify, 'item', recordObj, lineItem, isUpdate);
    });

    manageDiscountLine(options.procurify, recordObj, purchaseOrder, isUpdate); // Ensure this is negative
    manageFreightLine(options.procurify, recordObj, purchaseOrder, isUpdate);
    manageTaxLine(options.procurify, recordObj, purchaseOrder, isUpdate);
    manageOtherLine(options.procurify, recordObj, purchaseOrder, isUpdate);

    setAddress(recordObj, purchaseOrder.shipping_address, options.procurify, false);

    // Set the NetSuite billaddress to the incoming Vendor address (PFY)s
    setAddress(recordObj, {
        address_line_one: purchaseOrder.vendor.address_line_one,
        city: purchaseOrder.vendor.city,
        country: purchaseOrder.vendor.country,
        name: purchaseOrder.vendor.name,
        postal_code: purchaseOrder.vendor.postal_code,
        state_province: purchaseOrder.vendor.state_province
    }, options.procurify, true);

    if (purchaseOrder.shipping_method && purchaseOrder.shipping_method.name) {
        options.procurify.recordLookup({
            lookingForValue: undefined, // Only needed for Vendor Lookup
            searchOptions: {
                type: search.Type.SHIP_ITEM,
                filters: [['itemid', 'is', purchaseOrder.shipping_method.name]]
            },
            isMandatory: false,
            Found: shipMethodResult => {
                recordObj.setValue({fieldId: 'shipmethod', value: +shipMethodResult.id});
            }
        });
    }

    if (purchaseOrder.payment_term && purchaseOrder.payment_term.name) {
        options.procurify.recordLookup({
            lookingForValue: undefined, // Only needed for Vendor Lookup
            searchOptions: {
                type: search.Type.TERM,
                filters: [['name', 'is', purchaseOrder.payment_term.name]]
            },
            isMandatory: true,
            Found: termsResult => {
                recordObj.setValue({fieldId: 'terms', value: +termsResult.id});
            }
        });
    }

    // Incoterm is a non-searchable list (and customizaable by end user).  Best case is, we set via TEXT
    if (purchaseOrder.shipping_term && purchaseOrder.shipping_term.name) {
        // recordObj.setText({fieldId: 'incoterm', text: purchaseOrder.shipping_term.name}); // We're unable to accomodate this field at this moment
    }

    return recordObj;
};

const removeLine = (recordObj: record.Record, sublistId: string, procurifyLineId: number) => {
    const foundLineIndex: number = +recordObj.findSublistLineWithValue({sublistId: sublistId, fieldId: 'custcol_procurify_line_id', value: `${procurifyLineId}`});
    if (foundLineIndex >= 0) {
        recordObj.selectLine({sublistId: sublistId, line: foundLineIndex});
        // Check to see if this line has already been received against
        const quantityReceived: number = +recordObj.getCurrentSublistValue({sublistId: sublistId, fieldId: 'quantityreceived'});
        if (quantityReceived > 0) {
            throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - removeLine', message: 'You cannot removed a line that has already been received against'});
        } else {
            recordObj.removeLine({sublistId: sublistId, line: foundLineIndex});
        }
    }
};

export const update: IImportHandler.PurchaseOrderHandler.update = (options: IPurchaseOrderUpdateHandlerOptions) => {
    log.audit('NetSuitePurchaseOrderHandler - update - options', options);
    const purchaseOrder: PurchaseOrder = options.object;
    try {
        const recordObj = record.load({
            type: record.Type.PURCHASE_ORDER,
            id: options.recordId,
            isDynamic: true
        });

        const purchaseOrderId: number = +upsert(options, recordObj, true).save();
        options.Updated(purchaseOrderId);
    } catch (e) {
        options.Failed(typeof e === 'string' ? e : e.message ? e.message : JSON.stringify(e), purchaseOrder.id);
    }
};

// tslint:disable-next-line:variable-name
export const _delete: IImportHandler.PurchaseOrderHandler._delete = (options: IPurchaseOrderDeleteHandlerOptions) => {
    log.audit('NetSuitePurchaseOrderHandler - _delete - options', JSON.stringify(options, ((key, value) => key === 'procurify' ? undefined : value)));
    const purchaseOrder: PurchaseOrder = options.object;
    let poExists = false;

    search.create({
        type: search.Type.PURCHASE_ORDER,
        filters: [['internalidnumber', 'equalto', options.recordId], 'AND', ['mainline', 'is', 'T']]
    }).run().each(() => {
        poExists = true;
        return false; // Should only be one anyway
    });

    if (poExists) {
        // First, delete related Item Receipts
        search.create({
            type: search.Type.ITEM_RECEIPT,
            filters: [['createdfrom', 'anyof', options.recordId], 'AND', ['mainline', 'is', 'T']]
        }).run().each(result => {
            try {
                record.delete({
                    type: <string>result.recordType,
                    id: result.id
                });
            } catch (e) {
                // If we have Item Receipt failures, simply log and move on
                log.error(`Unable to delete Item Receipt: ${result.id}`, typeof e === 'string' ? e : e.message ? e.message : JSON.stringify(e));
            }
            return true;
        });

        try {
            record.delete({
                type: record.Type.PURCHASE_ORDER,
                id: options.recordId
            });
            options.Deleted();
        } catch (e) {
            options.Failed(typeof e === 'string' ? e : e.message ? e.message : JSON.stringify(e), purchaseOrder.id);
        }
    } else {
        log.error(`NetSuitePurchaseOrderHandler - _delete - Could not find PO: ${options.recordId}`, 'Marking as deleted (User likely manually deleted it)');
        options.Deleted();
    }
};

export const create: IImportHandler.PurchaseOrderHandler.create = (options: IPurchaseOrderCreateHandlerOptions) => {
    log.audit('NetSuitePurchaseOrderHandler - create - options', JSON.stringify(options, ((key, value) => key === 'procurify' ? undefined : value)));
    const purchaseOrder: PurchaseOrder = options.object;
    // Use the currency of the first line item, otherwise use USD
    const currencyCode: string = purchaseOrder.line_items && purchaseOrder.line_items[0].currency.name ? purchaseOrder.line_items[0].currency.name : 'USD';
    const currencyId: number = options.procurify.getCurrencyId(currencyCode);
    let currencyName: string;
    if (currencyId !== -1) {
        currencyName = options.procurify.getCurrencyNetsuiteName(currencyCode);
    }

    try {
        if (currencyId === -1) {
            throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - create', message: `NetSuite can not find the currency with the ISO code of ${purchaseOrder.line_items[0].currency.name}. Please ensure this currency exists in NetSuite.`});
        }

        const vendorSearchOptions: search.SearchCreateOptions = {
            type: search.Type.VENDOR,
            filters: getVendorLookupFilters(purchaseOrder.vendor.name.trim(), options.procurify)
        };
        // If you need additional information sourced from the Vendor, grab it in these columns for the search
        vendorSearchOptions.columns = ['isinactive'];
        if (options.procurify.Configuration.IsMultiCurrency) {
            if (options.procurify.Configuration.IsMultiCurrencyVendor) {
                vendorSearchOptions.columns.push('vendorcurrencybalance.currency');
            } else {
                vendorSearchOptions.columns.push('currency');
            }
        }

        // We may need to set a subsidairy, so lets get a value by default
        if (options.procurify.Configuration.IsOneWorld) {
            vendorSearchOptions.columns.push('subsidiarynohierarchy'); // This will return the Primary Subsidiary
        }

        log.audit('NetSuitePurchaseOrderHandler - create - vendorSearchOptions', vendorSearchOptions);

        const nsAndPfyNames = {
            netsuiteName: currencyName,
            procurifyName: currencyCode,
            vendorName: purchaseOrder.vendor.name.trim()
        };

        options.procurify.recordLookup({
            searchOptions: vendorSearchOptions,
            objectId: purchaseOrder.vendor.id,
            isMandatory: true,
            lookingForValue: nsAndPfyNames,
            Found: vendorResult => {
                log.audit('NetsuitePurchaseOrderHandler - Create - Found', vendorResult.getAllValues());
                if (vendorResult.getValue({name: 'isinactive'})) {
                    throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - create', message: `The vendor in NetSuite is currently inactive. Please activate the vendor in order to sync this purchase order`});
                }
                const recordObj = record.transform({
                    fromId: +vendorResult.id,
                    fromType: record.Type.VENDOR,
                    isDynamic: true,
                    toType: record.Type.PURCHASE_ORDER
                });

                if (options.procurify.Configuration.IsOneWorld) {
                    const configuredSubsidiaryIds: number[] = options.procurify.Configuration.SubsidiaryIds;
                    const vendorPrimarySubsidiaryId: number = +vendorResult.getValue({name: 'subsidiarynohierarchy'}); // Vendor's Primary subsidiary column

                    if (configuredSubsidiaryIds.length > 1) {
                        // Make sure the Vendor's Primary Subsidiary is listed in the Configured Subsidiaries
                        if (options.procurify.Configuration.SubsidiaryIds.indexOf(vendorPrimarySubsidiaryId) > -1) {
                            recordObj.setValue({fieldId: 'subsidiary', value: vendorPrimarySubsidiaryId}); // Use the Vendor's Primary subsidiary
                        } else {
                            throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - create', message: `If none of the connection's subsidiaries [${configuredSubsidiaryIds.join(',')}] match vendor's primary subsidiary [${vendorPrimarySubsidiaryId}]`});
                        }
                    } else if (configuredSubsidiaryIds.length === 1) {
                        recordObj.setValue({fieldId: 'subsidiary', value: configuredSubsidiaryIds[0]}); // Use the connections's only subsidiary
                    } else {
                        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - create', message: `This connection has no subsidiaries configured.`});
                    }
                }

                recordObj.setValue({fieldId: 'custbody_procurify_event', value: options.eventId});
                recordObj.setValue({fieldId: 'externalid', value: purchaseOrder.uuid});
                recordObj.setValue({fieldId: 'supervisorapproval', value: true});

                if (options.procurify.Configuration.IsMultiCurrency) {
                    recordObj.setValue({fieldId: 'currency', value: currencyId});
                }

                const url = `https://${options.procurify.Configuration.Domain}/#/purchase/order/details/${purchaseOrder.uuid.replace(/-/g, '')}`;
                const link = `<a href="${url}" target="_blank">${url}</a>`;
                recordObj.setValue({fieldId: 'custbody_procurify_linkback', value: link});

                const purchaseOrderId: number = +upsert(options, recordObj).save();
                options.Created(purchaseOrderId);
            }
        });
    } catch (e) {
        options.Failed(typeof e === 'string' ? e : e.message ? e.message : JSON.stringify(e), purchaseOrder.id);
    }
};

const upsertLine = (procurify: ProcurifySDK, sublistId: string, recordObj: record.Record, lineItem: LineItem, isUpdate = false) => {
    if (isUpdate) {
        const foundLineIndex: number = +recordObj.findSublistLineWithValue({sublistId: sublistId, fieldId: 'custcol_procurify_line_id', value: `${lineItem.id}`});
        if (foundLineIndex >= 0) {
            recordObj.selectLine({sublistId: sublistId, line: foundLineIndex});
            // Check to see if this line has already been received against
            const quantityReceived: number = +recordObj.getCurrentSublistValue({sublistId: sublistId, fieldId: 'quantityreceived'});
            const originalQuantity: number = +recordObj.getCurrentSublistValue({sublistId: sublistId, fieldId: 'quantity'});
            if (+lineItem.quantity !== originalQuantity && quantityReceived > 0) {
                throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - upsertLine', message: 'You cannot modify the quanity of a Purchase Order line that has already been received against'});
            }
        }
    } else {
        recordObj.selectNewLine({sublistId: sublistId});
    }

    if (lineItem.catalog_item && +lineItem.catalog_item.external_id > 0) {
        recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'item', value: +lineItem.catalog_item.external_id});
    } else {
        const procurifyAccountCode: string = <string>lineItem.account.code;
        const procurifyAccountId: number = lineItem.account.id;
        procurify.getNetSuiteAccountDetail({
            number: procurifyAccountCode,
            procurifyId: procurifyAccountId,
            configurationId: procurify.Configuration.Id,
            Found: netSuiteAccountDetail => {
                if (!netSuiteAccountDetail.isinactive) {
                    const accountItemId: number = netSuiteAccountDetail.custrecord_procurify_acct_item;
                    if (accountItemId > 0) {
                        recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'item', value: accountItemId});
                    } else {
                        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - upsertLine', message: `Account Code ${procurifyAccountCode}: does not have an associated Procurify Item`});
                    }
                } else {
                    throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - upsertLine', message: `Account Code ${procurifyAccountCode}: is inactive`});
                }
            },
            NotFound: () => {
                throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - upsertLine', message: `Unable to find Account [Code: ${procurifyAccountCode}] [Procurify ID: ${procurifyAccountId}]`});
            }
        });
    }

    recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'quantity', value: +lineItem.quantity});
    recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'rate', value: +lineItem.unit_cost});
    recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'amount', value: lineItem.amount});
    if (lineItem.department && lineItem.department.name) {
        // procurify.manageSegment({sublistId: sublistId, segment: search.Type.DEPARTMENT, name: lineItem.department.name.trim(), recordObj: recordObj});
    }
    if (lineItem.location && lineItem.location.name) {
        // procurify.manageSegment({sublistId: sublistId, segment: search.Type.LOCATION, name: lineItem.location.name.trim(), recordObj: recordObj});
    }
    // procurify.manageSegment({sublistId: sublistId, segment: search.Type.CLASSIFICATION, filters: [['name', 'is', 'test']], recordObj: recordObj}); // Based on CustomFields
    let description: string = lineItem.name;
    if (lineItem.memo && lineItem.memo.length > 0) {
        description += ` : ${lineItem.memo}`;
    }

    recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'description', value: description});

    recordObj.setCurrentSublistValue({sublistId: sublistId, fieldId: 'custcol_procurify_line_id', value: lineItem.id});

    recordObj.commitLine({sublistId: sublistId});
};

const manageDiscountLine = (procurify: ProcurifySDK, recordObj: record.Record, purchaseOrder: PurchaseOrder, isUpdate = false) => {
    if (procurify.Configuration.Discount_Item > 0) {
        manageMiscCost({
            procurify: procurify,
            sublistId: 'item',
            recordObj: recordObj,
            amount: purchaseOrder.discount,
            itemId: procurify.Configuration.Discount_Item,
            departmentId: procurify.Configuration.Discount_Department,
            classId: procurify.Configuration.Discount_Class,
            locationId: procurify.Configuration.Discount_Location,
            memo: 'Discount Line',
            lineId: -1
        }, isUpdate, true);
    } else {
        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - manageDiscountLine', message: 'Discount Item has not been set.  Please visit the Settings page'});
    }
};

const manageFreightLine = (procurify: ProcurifySDK, recordObj: record.Record, purchaseOrder: PurchaseOrder, isUpdate = false) => {
    if (procurify.Configuration.Freight_Item > 0) {
        manageMiscCost({
            procurify: procurify,
            sublistId: 'item',
            recordObj: recordObj,
            amount: purchaseOrder.freight,
            itemId: procurify.Configuration.Freight_Item,
            departmentId: procurify.Configuration.Freight_Department,
            classId: procurify.Configuration.Freight_Class,
            locationId: procurify.Configuration.Freight_Location,
            memo: 'Freight Line',
            lineId: -2
        }, isUpdate);
    } else {
        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - manageDiscountLine', message: 'Freight Item has not been set.  Please visit the Settings page'});
    }
};

const manageTaxLine = (procurify: ProcurifySDK, recordObj: record.Record, purchaseOrder: PurchaseOrder, isUpdate = false) => {
    if (procurify.Configuration.Tax_Item > 0) {
        manageMiscCost({
            procurify: procurify,
            sublistId: 'item',
            recordObj: recordObj,
            amount: purchaseOrder.tax,
            itemId: procurify.Configuration.Tax_Item,
            departmentId: procurify.Configuration.Tax_Department,
            classId: procurify.Configuration.Tax_Class,
            locationId: procurify.Configuration.Tax_Location,
            memo: 'Tax Line',
            lineId: -3
        }, isUpdate);
    } else {
        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - manageDiscountLine', message: 'Tax Item has not been set.  Please visit the Settings page'});
    }
};

const manageOtherLine = (procurify: ProcurifySDK, recordObj: record.Record, purchaseOrder: PurchaseOrder, isUpdate = false) => {
    if (procurify.Configuration.Other_Item > 0) {
        manageMiscCost({
            procurify: procurify,
            sublistId: 'item',
            recordObj: recordObj,
            amount: purchaseOrder.other,
            itemId: procurify.Configuration.Other_Item,
            departmentId: procurify.Configuration.Other_Department,
            classId: procurify.Configuration.Other_Class,
            locationId: procurify.Configuration.Other_Location,
            memo: 'Other Line',
            lineId: -4
        }, isUpdate);
    } else {
        throw error.create({name: 'CustomNetSuitePurchaseOrderHandler - manageDiscountLine', message: 'Other Item has not been set.  Please visit the Settings page'});
    }
};

const manageMiscCost = (options: AddMiscCostOptions, isUpdate = false, isDiscount = false) => {
    let foundLineIndex = -1;
    if (isUpdate) {
        foundLineIndex = +options.recordObj.findSublistLineWithValue({sublistId: options.sublistId, fieldId: 'custcol_procurify_line_id', value: `${options.lineId}`});
    }

    if (options.amount && +options.amount > 0) {
        if (foundLineIndex >= 0) {
            options.recordObj.selectLine({sublistId: options.sublistId, line: foundLineIndex});
        } else {
            options.recordObj.selectNewLine({sublistId: options.sublistId});
            options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'item', value: options.itemId});
        }
        const amount: number = isDiscount ? (+options.amount * -1) : +options.amount;
        options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'quantity', value: 1});
        options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'rate', value: amount});
        options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'amount', value: amount});
        // options.procurify.manageSegment({sublistId: options.sublistId, segment: search.Type.DEPARTMENT, recordObj: options.recordObj, segmentId: options.departmentId});
        // options.procurify.manageSegment({sublistId: options.sublistId, segment: search.Type.CLASSIFICATION, recordObj: options.recordObj, segmentId: options.classId});
        // options.procurify.manageSegment({sublistId: options.sublistId, segment: search.Type.LOCATION, recordObj: options.recordObj, segmentId: options.locationId});
        if (options.memo && options.memo.length > 0) {
            options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'description', value: options.memo});
        }
        options.recordObj.setCurrentSublistValue({sublistId: options.sublistId, fieldId: 'custcol_procurify_line_id', value: options.lineId});
        options.recordObj.commitLine({sublistId: options.sublistId});
    } else {
        if (foundLineIndex >= 0) {
            options.recordObj.removeLine({sublistId: options.sublistId, line: foundLineIndex});
        }
    }
};

interface AddMiscCostOptions {
    procurify: ProcurifySDK;
    sublistId: string;
    recordObj: record.Record;
    amount?: string;
    itemId: number;
    departmentId: number;
    classId: number;
    locationId: number;
    memo: string;
    lineId: number;
}

export const setAddress = (transaction: record.Record, address: Address, procurify: ProcurifySDK, isBilling = false): void => {
    if (address) {
        // To prevent the system from overriding the billing/shipping address defined
        // in this script with a default billing/shipping address (if one exists),
        // set the billaddresslist/shipaddresslist field to null.
        transaction.setValue({fieldId: isBilling ? 'billaddresslist' : 'shipaddresslist', value: null});

        const subAddress = transaction.getSubrecord({fieldId: isBilling ? 'billingaddress' : 'shippingaddress'});

        if (address.country) {
            procurify.getCountryCode({
                country: address.country,
                Found: countryCode => {
                    subAddress.setValue({fieldId: 'country', value: countryCode});
                },
                NotFound: () => {
                    log.error('setAddress', `No NetSuite Country found for ${address.country}`);
                }
            });
        }
        if (address.name) {
            subAddress.setValue({fieldId: 'attention', value: address.name});
        }
        if (address.address_line_one) {
            subAddress.setValue({fieldId: 'addr1', value: address.address_line_one});
        }
        if (address.address_line_two) {
            subAddress.setValue({fieldId: 'addr2', value: address.address_line_two});
        }
        if (address.city) {
            subAddress.setValue({fieldId: 'city', value: address.city});
        }
        if (address.state_province) {
            subAddress.setValue({fieldId: 'state', value: address.state_province});
        }
        if (address.postal_code) {
            subAddress.setValue({fieldId: 'zip', value: address.postal_code});
        }

        setBodyLevelAddress(transaction, address, procurify, isBilling);

    }
};

const setBodyLevelAddress = (transaction: record.Record, address: Address, procurify: ProcurifySDK, isBilling = false) => {
    if (address.country) {
        procurify.getCountryCode({
            country: address.country,
            Found: countryCode => {
                transaction.setValue({fieldId: isBilling ? 'billcountry' : 'shipcountry', value: countryCode});
            },
            NotFound: () => {
                log.error('setAddress', `No NetSuite Country found for ${address.country}`);
            }
        });
    }
    if (address.name) {
        transaction.setValue({fieldId: isBilling ? 'billattention' : 'shipattention', value: address.name});
    }
    if (address.address_line_one) {
        transaction.setValue({fieldId: isBilling ? 'billaddr1' : 'shipaddr1', value: address.address_line_one});
    }

    if (address.address_line_two) {
        transaction.setValue({fieldId: isBilling ? 'billaddr2' : 'shipaddr2', value: address.address_line_two});
    }

    if (address.city) {
        transaction.setValue({fieldId: isBilling ? 'billcity' : 'shipcity', value: address.city});
    }
    if (address.state_province) {
        transaction.setValue({fieldId: isBilling ? 'billstate' : 'shipstate', value: address.state_province});
    }
    if (address.postal_code) {
        transaction.setValue({fieldId: isBilling ? 'billzip' : 'shipzip', value: address.postal_code});
    }
};

// There's a a feature call MULTICURRENCYVENDOR that I need to check .. if not turned on, simply check against the 'currency' field on the vendor
const getVendorLookupFilters = (vendorName: string, procurify: ProcurifySDK): any => {
    const vendorFilters: any[] = [];
    vendorFilters.push(['formulatext: {companyname}', 'is', vendorName.trim()]);

    if (procurify.Configuration.IsOneWorld) {
        vendorFilters.push('AND');
        const subsidiaryIds: string[] = procurify.Configuration.SubsidiaryIds.map(String);
        vendorFilters.push(['msesubsidiary.internalid', 'anyof'].concat(subsidiaryIds));
    }
    return vendorFilters;
};

const getExistingLineIds = (transaction: record.Record, sublistId: string): number[] => {
    const foundPFYLineIds: number[] = [];
    for (let i = 0, lineCount: number = +transaction.getLineCount({sublistId: sublistId}); i < lineCount; i = i + 1) {
        const pfyLindId: number = +transaction.getSublistValue({sublistId: sublistId, fieldId: 'custcol_procurify_line_id', line: i});
        if (pfyLindId > 0) {
            foundPFYLineIds.push(pfyLindId);
        }
    }
    return foundPFYLineIds;
};
