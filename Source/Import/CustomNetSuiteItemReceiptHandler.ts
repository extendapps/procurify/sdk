/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import {IImportHandler, IItemReceiptCreateHandlerOptions, IItemReceiptDeleteHandlerOptions, IItemReceiptUpdateHandlerOptions} from '@extendapps/procurifytypings/Interfaces/IImportHandler';
import * as log from 'N/log';
import * as record from 'N/record';
import * as moment from '../moment';
import {ItemReceiptLine} from '@extendapps/procurifyapi/Objects/ItemReceiptLine';
import * as error from 'N/error';

export const update: IImportHandler.ItemReceiptHandler.update = (options: IItemReceiptUpdateHandlerOptions) => {
    log.audit('ItemReceipt - edit', options);
    options.Failed('Update not supported', -1);
};

// tslint:disable-next-line:variable-name
export const _delete: IImportHandler.ItemReceiptHandler._delete = (options: IItemReceiptDeleteHandlerOptions) => {
    log.audit('ItemReceipt - _delete', options);
    options.Deleted();
};

export const create: IImportHandler.ItemReceiptHandler.create = (options: IItemReceiptCreateHandlerOptions) => {
    log.audit('ItemReceipt - create', options);
    const itemReceiptLines: ItemReceiptLine[] = options.object;

    try {
        let recordObj: record.Record = null;
        const isUpdate: boolean = itemReceiptLines[0].external_id && +itemReceiptLines[0].external_id > 0;
        log.audit('ItemReceipt - create - isUpdate', isUpdate);

        if (isUpdate) {
            recordObj = record.load({
                type: record.Type.ITEM_RECEIPT,
                id: +itemReceiptLines[0].external_id,
                isDynamic: true
            });
        } else if (+itemReceiptLines[0].purchase_order.external_id > 0) {
            recordObj = record.transform({
                fromId: +itemReceiptLines[0].purchase_order.external_id,
                fromType: record.Type.PURCHASE_ORDER,
                isDynamic: true,
                toType: record.Type.ITEM_RECEIPT
            });
            recordObj.setValue({fieldId: 'trandate', value: options.procurify.procurifyDatetoNSDate(itemReceiptLines[0].updated_at)});
        }

        if (recordObj !== null) {
            recordObj.setValue({fieldId: 'custbody_procurify_event', value: options.eventId});
            const url = `https://${options.procurify.Configuration.Domain}/#/receive/po/${itemReceiptLines[0].purchase_order.uuid.replace(/-/g, '')}`;
            const link = `<a href="${url}" target="_blank">${url}</a>`;
            recordObj.setValue({fieldId: 'custbody_procurify_linkback', value: link});

            // Capture consolidated receive amounts
            const receiveLineInfo: { [pfyLineId: number]: number } = {};

            // #1 - Let's group them all by by Order Item Id (So, a target a single line/item)
            const groupedByOrderItemId = groupByOrderItemId(itemReceiptLines);

            for (const orderItemId in groupedByOrderItemId) {
                // #2 - In case there were edits to the same Item Receipt line in PFY within a 15 minute interval, let's grooup them by Id, sort them, and take the newest of them all
                const groupedByItemReceiptId: any = groupByItemReceiptId(groupedByOrderItemId[orderItemId]);

                for (const itemReceiptId in groupedByItemReceiptId) {
                    // noinspection JSUnfilteredForInLoop
                    const groupedItemReceiptLines: ItemReceiptLine[] = groupedByItemReceiptId[itemReceiptId];

                    // #3 - Sort them by date oldest to newest
                    const sortedItemReceiptLinesByDate: ItemReceiptLine[] = groupedItemReceiptLines.sort((a, b) => moment(a.updated_at).diff(moment(b.updated_at)));

                    // #4 - Grab that newest (the last one)
                    const newestItemReceiptLine: ItemReceiptLine = sortedItemReceiptLinesByDate.pop();

                    // #5 - If' we've already seen this order item id (line), then let's sum them up
                    if (receiveLineInfo[newestItemReceiptLine.order_item_id]) {
                        receiveLineInfo[newestItemReceiptLine.order_item_id] += calculateReceiveAmount(newestItemReceiptLine, isUpdate);
                    } else {
                        receiveLineInfo[newestItemReceiptLine.order_item_id] = calculateReceiveAmount(newestItemReceiptLine, isUpdate);
                    }
                }
            }

            // Let's make sure all the incoming PFY received lines are available in this NetSuite Item Receipt
            const missingLineIds: number[] = [];
            // #1: Collect all available 'custcol_procurify_line_id'
            const availableNetSuitePFYLineIds: number[] = [];
            for (let i = 0, lineCount: number = +recordObj.getLineCount('item'); i < lineCount; i = i + 1) {
                const pfyLineId: number = +recordObj.getSublistValue({sublistId: 'item', fieldId: 'custcol_procurify_line_id', line: i});
                if (pfyLineId > 0) {
                    availableNetSuitePFYLineIds.push(pfyLineId);
                }
            }
            // #2: Loop through incoming PFY received line ids and check if they are found
            for (const incomingPFYLineId in receiveLineInfo) {
                if (availableNetSuitePFYLineIds.indexOf(+incomingPFYLineId) === -1) {
                    // Ok, we found an incoming PFY receipt line that is not found on the NetSuite Item Receipt.  Need to report this.
                    missingLineIds.push(+incomingPFYLineId);
                }
            }

            if (missingLineIds.length === 0) {
                for (let i = 0, lineCount: number = +recordObj.getLineCount('item'); i < lineCount; i = i + 1) {
                    const pfyLineId: number = +recordObj.getSublistValue({sublistId: 'item', fieldId: 'custcol_procurify_line_id', line: i});
                    const quantityRemaining: number = +recordObj.getSublistValue({sublistId: 'item', fieldId: 'quantityremaining', line: i});
                    const currentlyReceivedAmt: number = +recordObj.getSublistValue({sublistId: 'item', fieldId: 'quantity', line: i});

                    let incomingReceiveQty = 0;

                    if (receiveLineInfo[pfyLineId]) {
                        incomingReceiveQty = +receiveLineInfo[pfyLineId];
                    }

                    if (incomingReceiveQty !== 0) {
                        if (incomingReceiveQty > 0) {
                            // Simply update the IR line and set to receive
                            updateLine({recordObj: recordObj, lineIndex: i, quantity: incomingReceiveQty, quantityRemaining: quantityRemaining, isReceiving: true});
                        } else if (incomingReceiveQty < 0) {
                            const netReceivedAmt: number = currentlyReceivedAmt + incomingReceiveQty;
                            if (netReceivedAmt > 0) {
                                // Reduce the receive amount
                                updateLine({recordObj: recordObj, lineIndex: i, quantity: netReceivedAmt, quantityRemaining: quantityRemaining, isReceiving: true});
                            } else if (netReceivedAmt < 0) {
                                // We can't reduce this past zero, throw an error
                                // noinspection ExceptionCaughtLocallyJS
                                throw error.create({name: 'CustomNetSuiteItemReceiptHandler', message: `You attempted to reduce line: ${i} by ${incomingReceiveQty}, which is greater than ${currentlyReceivedAmt}`});
                            } else {
                                updateLine({recordObj: recordObj, lineIndex: i, quantity: 0, quantityRemaining: quantityRemaining, isReceiving: false});
                            }
                        } else {
                            // Do nothing as the net result is zero
                        }
                    } else {
                        if (!isUpdate) {
                            updateLine({recordObj: recordObj, lineIndex: i, quantity: 0, quantityRemaining: quantityRemaining, isReceiving: false});
                        } else {
                            log.audit('Untouched', `Leaving line ${i} alone`);
                        }
                    }
                }

                // Last loop to see if any lines are being received, if not .. either don't save OR delete the Item Receipt.
                let saveItemReceipt = false;
                for (let i = 0, lineCount: number = +recordObj.getLineCount('item'); i < lineCount; i = i + 1) {
                    const beingReceived: boolean = <boolean>recordObj.getSublistValue({sublistId: 'item', fieldId: 'itemreceive', line: i});
                    if (beingReceived) {
                        saveItemReceipt = true;
                        i = lineCount + 1; // We found at least one line to receive, let's break out of the loop.
                    }
                }

                if (saveItemReceipt) {
                    options.Created(+recordObj.save());
                } else {
                    if (isUpdate) {
                        log.audit(`Deleting Item Receipt ${+itemReceiptLines[0].external_id}`, `Nothing is being received anymore`);
                        // Delete the existing Item Rceipt
                        record.delete({
                            type: record.Type.ITEM_RECEIPT,
                            id: +itemReceiptLines[0].external_id
                        });
                        options.Created(+itemReceiptLines[0].external_id);
                    } else {
                        log.audit(`Doing nothing`, `Nothing has changed`);
                        // Don't save the Item Receipt at all
                        options.Created(-1);
                    }
                }
            } else {
                options.Failed(`Unable to match all incoming PFY Line Ids.  Missing Procurify Line Ids ${missingLineIds}`, itemReceiptLines[0].id);
            }
        } else {
            options.Failed('Failed to create/load Item Receipt', itemReceiptLines[0].id);
        }

    } catch (e) {
        if (e.message && e.message.indexOf('You can not initialize itemreceipt: invalid reference') !== -1) {
            options.Failed(`The NetSuite Purchase Order with internal ID ${itemReceiptLines[0].purchase_order.external_id} is either missing or already received. Please ensure that this Purchase Order, that is related to this Procurify Item Receipt, exists in NetSuite and that the items on this PO haven't already been received in NetSuite.`, itemReceiptLines[0].id);
            return;
        }
        options.Failed(typeof e === 'string' ? e : e.message ? e.message : JSON.stringify(e), itemReceiptLines[0].id);
    }
};

interface UpdateLineOptions {
    recordObj: record.Record;
    lineIndex: number;
    quantity: number;
    quantityRemaining: number;
    isReceiving: boolean;
}

const updateLine = (options: UpdateLineOptions) => {
    log.debug(`Updating line ${options.lineIndex}`, JSON.stringify(options, (key, value) => {
        if (key === 'recordObj') {
            return undefined;
        } else {
            return value;
        }
    }));
    options.recordObj.selectLine({sublistId: 'item', line: options.lineIndex});

    if (options.quantity === 0) {
        // Therefore, we're simply NOT going to receive this line
        options.isReceiving = false;
    }

    options.recordObj.setCurrentSublistValue({sublistId: 'item', fieldId: 'itemreceive', value: options.isReceiving});

    if (options.isReceiving) {
        // Check against the quantityremaining value (only avaaible in a NEW ITEM RECEIPT) and ensure the quantity is equal or less then
        if (options.quantity <= options.quantityRemaining) {
            options.recordObj.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
                value: `${options.quantity}`
            });
        } else {
            throw error.create({name: 'CustomNetSuiteItemReceiptHandler - updateLine', message: `You are attempting to receive a quantity of items (${options.quantity}) greater than than allowed on the referenced Purchase Order (${options.quantityRemaining}). Please review the referenced Purchase Order quantities and adjust.`});
        }
    }
    options.recordObj.commitLine({sublistId: 'item'});
};

const calculateReceiveAmount = (itemReceiptLine: ItemReceiptLine, isUpdate: boolean): number => {
    let receiveAmount: number;
    // tslint:disable-next-line:prefer-conditional-expression
    if (+itemReceiptLine.received_quantity > 0) {
        receiveAmount = +itemReceiptLine.received_quantity;
    } else if (isUpdate) {
        // Grab the 2nd last receive_history received_quantity
        receiveAmount = (+itemReceiptLine.receive_history[itemReceiptLine.receive_history.length - 2].received_quantity) * -1;
    } else {
        receiveAmount = 0;
    }
    if (receiveAmount === null) {
        throw error.create({name: 'CustomNetSuiteItemReceiptHandler - calculateReceiveAmount', message: 'Something went wrong calculating the receiveAmount'});
    }
    return receiveAmount;
};

const groupByOrderItemId = (itemReceiptLines: ItemReceiptLine[]) => {
    return itemReceiptLines.reduce((orderItemIdGroup, itemReceiptLine) => {
        const orderItemId = itemReceiptLine.order_item_id;

        if (orderItemIdGroup[orderItemId] === undefined) {
            orderItemIdGroup[orderItemId] = [];
        }

        orderItemIdGroup[orderItemId].push(itemReceiptLine);
        return orderItemIdGroup;
    }, {});
};

const groupByItemReceiptId = (itemReceiptLines: ItemReceiptLine[]) => {
    return itemReceiptLines.reduce((itemReceiptGroup, itemReceiptLine) => {
        const itemReceiptId = itemReceiptLine.id;

        if (itemReceiptGroup[itemReceiptId] === undefined) {
            itemReceiptGroup[itemReceiptId] = [];
        }

        itemReceiptGroup[itemReceiptId].push(itemReceiptLine);
        return itemReceiptGroup;
    }, {});
};
