/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {IBaseExportAccountHandlerOptions, IExportHandler} from '@extendapps/procurifytypings/Interfaces/IExportHandler';
import {Account} from '@extendapps/procurifyapi/Objects/Account';

export const handler: IExportHandler.AccountHandler.handler = (options: IBaseExportAccountHandlerOptions) => {
    log.audit('Account Handler - netSuiteRecord', options.netSuiteRecord);

    const procurifyAccount: Account = {
        code: <string>options.netSuiteRecord.getValue({fieldId: 'acctnumber'}),
        description: <string>options.netSuiteRecord.getValue({fieldId: 'acctname'}),
        account_type: options.procurify.getAccountType(<string>options.netSuiteRecord.getValue({fieldId: 'accttype'})),
        active: options.netSuiteRecord.getValue({fieldId: 'isinactive'}) === false,
        external_id: +options.netSuiteRecord.id
    };

    options.Send(procurifyAccount);
};
