/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Diego@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import * as url from 'N/url';
import * as search from 'N/search';
import * as record from 'N/record';
import {IBaseExportCatalogItemHandlerOptions, IExportHandler} from '@extendapps/procurifytypings/Interfaces/IExportHandler';
import {CatalogItem} from '@extendapps/procurifyapi/Objects/CatalogItem';

export const handler: IExportHandler.CatalogItemHandler.handler = (options: IBaseExportCatalogItemHandlerOptions) => {
    log.audit('Payment Catalog Item Handler - netSuiteRecord', options.netSuiteRecord);
    const domain = options.procurify.Configuration.Domain;
    let acctField: string;

    switch (options.netSuiteRecord.type) {
        case record.Type.INVENTORY_ITEM:
        case record.Type.SERIALIZED_INVENTORY_ITEM:
        case record.Type.LOT_NUMBERED_INVENTORY_ITEM:
        case record.Type.ASSEMBLY_ITEM:
        case record.Type.SERIALIZED_ASSEMBLY_ITEM:
        case record.Type.LOT_NUMBERED_ASSEMBLY_ITEM:
            acctField = 'assetaccount';
            break;
        case record.Type.SERVICE_ITEM:
        case record.Type.NON_INVENTORY_ITEM:
            acctField = 'expenseaccount';
            break;
        default:
            break;
    }

    const acctRecordId = options.netSuiteRecord.getValue({fieldId: acctField});
    let procurifyAccId = -1;
    search.create({
        type: 'customrecord_procurify_object',
        filters: [
            ['custrecord_procurify_object_account', 'anyof', acctRecordId],
            'AND',
            ['custrecord_procurify_object_domain', 'is', domain]
        ],
        columns: [
            'custrecord_procurify_object_id'
        ]
    }).run().each(result => {
        procurifyAccId = +result.getValue(result.columns[0]);
        return false;
    });

    if (procurifyAccId < 0) {
        options.Failed({
            code: 400,
            headers: {},
            body: '{"data":{},"metadata":{},"errors":{"account_code":{"code":"NOT_SYNCED_ACCOUNT","message":"The selected account does not exist in Procurify."}}}'
        });
        return false;
    }

    let fullName: string;
    let internalSku: string;
    const firstName = <string>options.netSuiteRecord.getValue({fieldId: 'itemid'});
    const secondHalfName = <string>options.netSuiteRecord.getValue({fieldId: 'displayname'});

    if ((secondHalfName !== '') && (firstName !== secondHalfName)) {
        fullName = secondHalfName;
        internalSku = firstName;
    } else {
        fullName = firstName;
        internalSku = null;
    }

    const preferredVendorLine = options.netSuiteRecord.findSublistLineWithValue({sublistId: 'itemvendor', fieldId: 'preferredvendor', value: 'T'});
    let vendorPfyId = -1;

    if (preferredVendorLine >= 0) {
        const vendorId = options.netSuiteRecord.getSublistValue({sublistId: 'itemvendor', fieldId: 'vendor', line: preferredVendorLine});
        search.create({
            type: 'customrecord_procurify_object',
            filters: [
                ['custrecord_procurify_object_vendor', 'anyof', vendorId],
                'AND',
                ['custrecord_procurify_object_domain', 'is', domain]
            ],
            columns: [
                'custrecord_procurify_object_id'
            ]
        }).run().each(result => {
            vendorPfyId = +result.getValue(result.columns[0]);
            return false;
        });
    }

    const procurifyCatalogItem: CatalogItem = {
        name: fullName,
        unit_type: <string>options.netSuiteRecord.getText({fieldId: 'purchaseunit'}),
        vendor: vendorPfyId > 0 ? vendorPfyId : null,
        account_code: procurifyAccId,
        internal_sku: internalSku,
        description: <string>options.netSuiteRecord.getValue({fieldId: 'purchasedescription'}),
        product_url: `https://${url.resolveDomain({hostType: url.HostType.APPLICATION})}/app/common/item/item.nl?id=${options.netSuiteRecord.id}`,
        currency: null,
        cost: <string>options.netSuiteRecord.getValue({fieldId: 'cost'}),
        external_id: `${options.netSuiteRecord.id}`
    };
    options.Send(procurifyCatalogItem);
};
