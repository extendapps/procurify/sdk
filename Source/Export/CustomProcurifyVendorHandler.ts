/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import * as search from 'N/search';
import {IBaseExportVendorHandlerOptions, IExportHandler} from '@extendapps/procurifytypings/Interfaces/IExportHandler';
import {Vendor} from '@extendapps/procurifyapi/Objects/Vendor';

export const handler: IExportHandler.VendorHandler.handler = (options: IBaseExportVendorHandlerOptions) => {
    log.audit('Vendor Handler - netSuiteRecord', options.netSuiteRecord);

    const isPerson: boolean = <string>options.netSuiteRecord.getValue({fieldId: 'isperson'}) === 'T';
    const procurifyVendor: Vendor = {
        name: isPerson ? `${options.netSuiteRecord.getValue({fieldId: 'firstname'})} ${options.netSuiteRecord.getValue({fieldId: 'lastname'})}` : <string>options.netSuiteRecord.getValue({fieldId: 'companyname'}),
        address_line_one: null,
        postal_code: null,
        city: null,
        state_province: null,
        country: null,
        email: <string>options.netSuiteRecord.getValue({fieldId: 'email'}),
        contact: <string>options.netSuiteRecord.getValue({fieldId: 'contact'}),
        phone: <string>options.netSuiteRecord.getValue({fieldId: 'phone'}),
        alt_phone: <string>options.netSuiteRecord.getValue({fieldId: 'altphone'}),
        fax: <string>options.netSuiteRecord.getValue({fieldId: 'fax'}),
        comments: <string>options.netSuiteRecord.getValue({fieldId: 'comments'}),
        url: <string>options.netSuiteRecord.getValue({fieldId: 'url'}),
        payment_term: <string>options.netSuiteRecord.getText({fieldId: 'terms'}),
        shipping_term: <string>options.netSuiteRecord.getText({fieldId: 'incoterm'}),
        alt_email: <string>options.netSuiteRecord.getText({fieldId: 'altemail'}),
        active: options.netSuiteRecord.getValue({fieldId: 'isinactive'}) === false,
        external_id: +options.netSuiteRecord.id,
        vendor_external_id: <string>options.netSuiteRecord.getValue({fieldId: 'entityid'})
    };

    // If procurifyVendor.name === procurifyVendor.vendor_external_id, then do NOT send the vendor_external_id
    if (procurifyVendor.name === procurifyVendor.vendor_external_id) {
        delete procurifyVendor.vendor_external_id;
    }

    const billingAddressLineId: number = +options.netSuiteRecord.findSublistLineWithValue({sublistId: 'addressbook', fieldId: 'defaultbilling', value: 'T'});
    if (billingAddressLineId >= 0) {
        options.netSuiteRecord.selectLine({sublistId: 'addressbook', line: billingAddressLineId});
        const address = options.netSuiteRecord.getCurrentSublistSubrecord({sublistId: 'addressbook', fieldId: 'addressbookaddress'});
        procurifyVendor.address_line_one = <string>address.getValue({fieldId: 'addr1'});
        procurifyVendor.address_line_two = <string>address.getValue({fieldId: 'addr2'});
        procurifyVendor.postal_code = <string>address.getValue({fieldId: 'zip'});
        procurifyVendor.city = <string>address.getValue({fieldId: 'city'});
        procurifyVendor.state_province = <string>address.getText({fieldId: 'state'});
        procurifyVendor.country = <string>address.getText({fieldId: 'country'});
        if (!procurifyVendor.phone || !(procurifyVendor.phone.length > 0)) {
            procurifyVendor.phone = <string>address.getValue({fieldId: 'addrphone'});
        }
    } else {
        delete procurifyVendor.address_line_one;
        delete procurifyVendor.postal_code;
        delete procurifyVendor.city;
        delete procurifyVendor.state_province;
        delete procurifyVendor.country;
    }

    // Find the vendor's Primary Contact
    search.create({
        type: search.Type.CONTACT,
        filters: [
            ['role', 'anyof', '-10'], // -10 is Primary Contact Role in NetSuite
            'AND',
            ['vendorprimary.internalid', 'anyof', options.netSuiteRecord.id]
        ],
        columns: [
            'entityid'
        ]
    }).run().each(result => {
        procurifyVendor.contact = <string>result.getValue(result.columns[0]);
        return false; // Should only ever be one Primary
    });

    options.Send(procurifyVendor);
};
