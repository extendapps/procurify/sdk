/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {IBaseExportPaymentTermHandlerOptions, IExportHandler} from '@extendapps/procurifytypings/Interfaces/IExportHandler';
import {PaymentTerm} from '@extendapps/procurifyapi/Objects/PaymentTerm';

export const handler: IExportHandler.PaymentTermHandler.handler = (options: IBaseExportPaymentTermHandlerOptions) => {
    log.audit('Payment Term Handler - netSuiteRecord', options.netSuiteRecord);

    const procurifyPaymentTerm: PaymentTerm = {
        name: <string>options.netSuiteRecord.getValue({fieldId: 'name'}),
        description: <string>options.netSuiteRecord.getValue({fieldId: 'name'}),
        external_id: +options.netSuiteRecord.id
    };

    options.Send(procurifyPaymentTerm);
};
