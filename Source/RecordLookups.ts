/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */

import * as cache from 'N/cache';

type NameMap = Record<string, number>;

/* tslint:disable */
const smartLookup = (lookupValue: string, cacheName: string, nameMaploader: () => NameMap): number => {
    const cachedNameMap: NameMap = JSON.parse(cache.getCache({name: cacheName, scope: cache.Scope.PROTECTED}).get({
        key: `${cacheName}_KEY`,
        loader: () => {
            return JSON.stringify(nameMaploader());
        },
        ttl: 300
    }));
    return cachedNameMap[lookupValue];
};
